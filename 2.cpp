#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<iostream>

#include <windows.h>
#include <glut.h>

using namespace std;

#define pi (2*acos(0.0))

double cameraHeight;
double cameraAngle;
int drawgrid;
int drawaxes;
double angle;


struct point
{
	double x, y, z;
};

struct vector
{
	double x, y, z;
};

double** genRotationMatrix(vector basis, double rotAngle)
{
	double** ret = new double*[3];
	for (int i = 0; i < 3; i++)
	{
		ret[i] = new double[3];
	}
	double c = cos(rotAngle);
	double s = sin(rotAngle);
	ret[0][0] = c + (basis.x * basis.x) * (1 - c);
	ret[0][1] = (basis.x * basis.y * (1 - c)) - (basis.z * s);
	ret[0][2] = (basis.y * s) + (basis.x * basis.z * (1 - c));
	ret[1][0] = (basis.z * s) + (basis.x * basis.y * (1 - c));
	ret[1][1] = (c + (basis.y * basis.y) * (1 - c));
	ret[1][2] = -(basis.x * s) + (basis.y * basis.z * (1 - c));
	ret[2][0] = -(basis.y * s) + (basis.x * basis.z * (1 - c));
	ret[2][1] = (basis.x * s) + (basis.y * basis.z *(1 - c));
	ret[2][2] = c + (basis.z * basis.z) * (1 - c);
	/*for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << ret[i][j];
		}
		cout << endl;
	}*/
	return ret;
}

vector rotate(vector v, vector basis, double angle)
{
	double** rotMat = genRotationMatrix(basis, angle);
	vector ret;
	//std::cout << "( " << v.x << " , " << v.y << " , " << v.z << " )" << endl;
	ret.x = rotMat[0][0] * v.x + rotMat[0][1] * v.y + rotMat[0][2] * v.z;
	ret.y = rotMat[1][0] * v.x + rotMat[1][1] * v.y + rotMat[1][2] * v.z;
	ret.z = rotMat[2][0] * v.x + rotMat[2][1] * v.y + rotMat[2][2] * v.z;
	//std::cout << "( " << ret.x << " , " << ret.y << " , " << ret.z << " )" << endl;
	for (int i = 0; i < 3; i++)
	{
		delete rotMat[i];
	}
	delete rotMat;
	return ret;
}


void drawAxes()
{
	if(drawaxes==1)
	{
		glBegin(GL_LINES);{
			glColor3f(1.0, 0,0);
			glVertex3f( 100,0,0);
			glVertex3f(-100,0,0);
			glColor3f(0, 1.0, 0);
			glVertex3f(0,-100,0);
			glVertex3f(0, 100,0);
			glColor3f(0, 0, 1.0);
			glVertex3f(0,0, 100);
			glVertex3f(0,0,-100);
		}glEnd();
	}
}


void drawGrid()
{
	int i;
	if(drawgrid==1)
	{
		glColor3f(0.6, 0.6, 0.6);	//grey
		glBegin(GL_LINES);{
			for(i=-8;i<=8;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i*10, -90, 0);
				glVertex3f(i*10,  90, 0);

				//lines parallel to X-axis
				glVertex3f(-90, i*10, 0);
				glVertex3f( 90, i*10, 0);
			}
		}glEnd();
	}
}

void drawSquare(double a)
{
    glColor3f(0.0,0.0,1.0);
	glBegin(GL_QUADS);{
		glVertex3f( a, a,0);
		glVertex3f( a,-a,0);
		glVertex3f(-a,-a,0);
		glVertex3f(-a, a,0);
	}glEnd();
	glColor3d(1, 1, 1);
	glBegin(GL_LINES);
	{
		glVertex3d(a, a, 0);
		glVertex3d(-a, a, 0);
		glVertex3d(a, a, 0);
		glVertex3d(a, -a, 0);
		glVertex3d(-a, a, 0);
		glVertex3d(-a, -a, 0);
		glVertex3d(-a, -a, 0);
		glVertex3d(a, -a, 0);
	}glEnd();
}


void drawCircle(double radius,int segments)
{
    int i;
    struct point points[100];
    glColor3f(0.7,0.7,0.7);
    //generate points
    for(i=0;i<=segments;i++)
    {
        points[i].x=radius*cos(((double)i/(double)segments)*2*pi);
        points[i].y=radius*sin(((double)i/(double)segments)*2*pi);
    }
    //draw segments using generated points
    for(i=0;i<segments;i++)
    {
        glBegin(GL_LINES);
        {
			glVertex3f(points[i].x,points[i].y,0);
			glVertex3f(points[i+1].x,points[i+1].y,0);
        }
        glEnd();
    }
}

void drawCone(double radius,double height,int segments)
{
    int i;
    double shade;
    struct point points[100];
    //generate points
    for(i=0;i<=segments;i++)
    {
        points[i].x=radius*cos(((double)i/(double)segments)*2*pi);
        points[i].y=radius*sin(((double)i/(double)segments)*2*pi);
    }
    //draw triangles using generated points
    for(i=0;i<segments;i++)
    {
        //create shading effect
        if(i<segments/2)shade=2*(double)i/(double)segments;
        else shade=2*(1.0-(double)i/(double)segments);
        glColor3f(shade,shade,shade);

        glBegin(GL_TRIANGLES);
        {
            glVertex3f(0,0,height);
			glVertex3f(points[i].x,points[i].y,0);
			glVertex3f(points[i+1].x,points[i+1].y,0);
        }
        glEnd();
    }
}


void drawCylinder(double radius, double height, int slices, int stacks)
{
	struct point** points = new point*[stacks+1];
	for (int i = 0; i <= stacks; i++)
	{
		points[i] = new point[slices + 1];
	}
	for (int i = 0; i <= stacks; i++)
	{
		for (int j = 0; j <= slices; j++)
		{
			points[i][j].x = radius*cos(((double)j / (double)slices)*(2 * pi));
			points[i][j].y = radius*sin(((double)j / (double)slices)*(2 * pi));
			points[i][j].z = (((double)i / (double)stacks)*height);
		}
	}
	for (int i = 0; i < stacks; i++)
	{
		glColor3d((double)i / (double)stacks, (double)i / (double)stacks, (double)i / (double)stacks);
		for (int j = 0; j < slices; j++)
		{
			glBegin(GL_QUADS);
			{
				glVertex3d(points[i][j].x, points[i][j].y, points[i][j].z);
				glVertex3d(points[i][j + 1].x, points[i][j + 1].y, points[i][j + 1].z);
				glVertex3d(points[i + 1][j + 1].x, points[i + 1][j + 1].y, points[i + 1][j + 1].z);
				glVertex3d(points[i + 1][j].x, points[i + 1][j].y, points[i + 1][j].z);
			}glEnd();
		}
	}
	for (int i = 0; i <= stacks; i++)
	{
		delete points[i];
	}
	delete points;
}

void drawSphere(double radius,int slices,int stacks)
{
	struct point points[100][100];
	int i,j;
	double h,r;
	//generate points
	for(i=0;i<=stacks;i++)
	{
		h=radius*sin(((double)i/(double)stacks)*(pi/2));
		r=radius*cos(((double)i/(double)stacks)*(pi/2));
		for(j=0;j<=slices;j++)
		{
			points[i][j].x=r*cos(((double)j/(double)slices)*(2*pi));
			points[i][j].y=r*sin(((double)j/(double)slices)*(2*pi));
			points[i][j].z=h;
		}
	}
	//draw quads using generated points
	for(i=0;i<stacks;i++)
	{
        glColor3f((double)i/(double)stacks,(double)i/(double)stacks,(double)i/(double)stacks);
		for(j=0;j<slices;j++)
		{
			glBegin(GL_QUADS);{
			    //upper hemisphere
				glVertex3f(points[i][j].x,points[i][j].y,points[i][j].z);
				glVertex3f(points[i][j+1].x,points[i][j+1].y,points[i][j+1].z);
				glVertex3f(points[i+1][j+1].x,points[i+1][j+1].y,points[i+1][j+1].z);
				glVertex3f(points[i+1][j].x,points[i+1][j].y,points[i+1][j].z);
                //lower hemisphere
                glVertex3f(points[i][j].x,points[i][j].y,-points[i][j].z);
				glVertex3f(points[i][j+1].x,points[i][j+1].y,-points[i][j+1].z);
				glVertex3f(points[i+1][j+1].x,points[i+1][j+1].y,-points[i+1][j+1].z);
				glVertex3f(points[i+1][j].x,points[i+1][j].y,-points[i+1][j].z);
			}glEnd();
		}
	}
}

void drawTorus(double innerRadius, double outerRadius, int stacks, int slices)
{
	//struct point* cPoints = new point[stacks + 1];
	struct point** points = new point*[stacks + 1];
	for (int i = 0; i <= stacks; i++)
	{
		points[i] = new point[slices + 1];
	}
	//double h, r;
	/*for (int i = 0; i <= stacks; i++)
	{
		cPoints[i].z = ((outerRadius-innerRadius)/2)*sin(((double)i / (double)stacks)*(pi));
		cPoints[i].y = 0;
		cPoints[i].x = ((outerRadius - innerRadius)/2)*cos(((double)i / (double)stacks)*(pi)) + (outerRadius+innerRadius)/2;
	}*/
	double radius;
	for (int i = 0; i <= stacks; i++)
	{
		radius = ((outerRadius - innerRadius) / 2)*cos(((double)i / (double)stacks)*(pi)) + (outerRadius + innerRadius) / 2;
		for (int j = 0; j <= slices; j++)
		{
			points[i][j].z = ((outerRadius - innerRadius) / 2)*sin(((double)i / (double)stacks)*(pi));
			points[i][j].x = radius*cos(((double)j / (double)slices)*(2*pi));
			points[i][j].y = radius*sin(((double)j / (double)slices)*(2*pi));
		}
	}
	/*for (int j = 0; j < slices; j++)
	{
		for (int i = 0; i < stacks; i++)
		{
			glBegin(GL_LINES); {
				glVertex3d(points[i][j].x, points[i][j].y, points[i][j].z);
				glVertex3d(points[i + 1][j].x, points[i + 1][j].y, points[i + 1][j].z);
				glVertex3d(points[i][j].x, points[i][j].y, -points[i][j].z);
				glVertex3d(points[i + 1][j].x, points[i + 1][j].y, -points[i + 1][j].z);
			}glEnd();
		}
	}*/
	double color = 1;
	for (int i = 0; i < stacks; i++)
	{
		glColor3d(color, color, color);
		color = 1 - color;
		for (int j = 0; j < slices; j++)
		{
			glBegin(GL_QUADS); {
				//upper hemisphere
				glVertex3f(points[i][j].x, points[i][j].y, points[i][j].z);
				glVertex3f(points[i][j + 1].x, points[i][j + 1].y, points[i][j + 1].z);
				glVertex3f(points[i + 1][j + 1].x, points[i + 1][j + 1].y, points[i + 1][j + 1].z);
				glVertex3f(points[i + 1][j].x, points[i + 1][j].y, points[i + 1][j].z);
				//lower hemisphere
				glVertex3f(points[i][j].x, points[i][j].y, -points[i][j].z);
				glVertex3f(points[i][j + 1].x, points[i][j + 1].y, -points[i][j + 1].z);
				glVertex3f(points[i + 1][j + 1].x, points[i + 1][j + 1].y, -points[i + 1][j + 1].z);
				glVertex3f(points[i + 1][j].x, points[i + 1][j].y, -points[i + 1][j].z);

			}glEnd();
		}
	}
	for (int i = 0; i <= stacks; i++)
	{
		delete points[i];
	}
	delete points;
}

void drawCircleZ(double innerRadius, double outerRadius, int stacks)
{
	struct point* points = new point[stacks + 1];
//	double h, r;
	for (int i = 0; i <= stacks; i++)
	{
		points[i].z = ((outerRadius - innerRadius)/2)*sin(((double)i / (double)stacks)*(pi));
		points[i].y = 0;
		points[i].x = ((outerRadius - innerRadius)/2)*cos(((double)i / (double)stacks)*(pi)) + (innerRadius+outerRadius)/2;
	}
	for (int i = 0; i < stacks; i++)
	{
		glBegin(GL_LINES); {
			glVertex3d(points[i].x, points[i].y, points[i].z);
			glVertex3d(points[i + 1].x, points[i + 1].y, points[i + 1].z);
			glVertex3d(points[i].x, points[i].y, -points[i].z);
			glVertex3d(points[i + 1].x, points[i + 1].y, -points[i + 1].z);
		}glEnd();
	}

	delete points;
}

void drawSS()
{
	glColor3f(1, 0, 0);
	drawSquare(20);

	glRotatef(angle, 0, 0, 1);
	glTranslatef(110, 0, 0);
	glRotatef(2 * angle, 0, 0, 1);
	glColor3f(0, 1, 0);
	drawSquare(15);

	glPushMatrix();
	{
		glRotatef(angle, 0, 0, 1);
		glTranslatef(60, 0, 0);
		glRotatef(2 * angle, 0, 0, 1);
		glColor3f(0, 0, 1);
		drawSquare(10);
	}
	glPopMatrix();

	glRotatef(3 * angle, 0, 0, 1);
	glTranslatef(40, 0, 0);
	glRotatef(4 * angle, 0, 0, 1);
	glColor3f(1, 1, 0);
	drawSquare(5);
}


double torusOuterR = 50;
double torusInnerR = 30;


void keyboardListener(unsigned char key, int x,int y){
	switch(key){
		case '1':
			if (torusInnerR < torusOuterR)
			{
				torusInnerR++;
			}
			break;
		case '2':
			if (torusInnerR > 0)
			{
				torusInnerR--;
			}
			break;
		case '3':
			torusOuterR++;
			break;
		case '4':
			if (torusOuterR > torusInnerR)
			{
				torusOuterR--;
			}
			break;
		case '-':
			drawgrid=1-drawgrid;
			break;
		default:
			break;
	}
}


void specialKeyListener(int key, int x,int y){
	switch(key){
		case GLUT_KEY_DOWN:		//down arrow key
			cameraHeight -= 3.0;
			break;
		case GLUT_KEY_UP:		// up arrow key
			cameraHeight += 3.0;
			break;

		case GLUT_KEY_RIGHT:
			cameraAngle += 0.03;
			break;
		case GLUT_KEY_LEFT:
			cameraAngle -= 0.03;
			break;

		default:
			break;
	}
}


void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				drawaxes=1-drawaxes;
			}
			break;

		case GLUT_RIGHT_BUTTON:
			//........
			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}




void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?

	//gluLookAt(100,100,100,	0,0,0,	0,0,1);
	gluLookAt(200*cos(cameraAngle), 200*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
	//gluLookAt(0,0,200,	0,0,0,	0,1,0);
	//gluLookAt(pos.x, pos.y, pos.z, pos.x + l.x, pos.y + l.y, pos.z + l.z, u.x, u.y, u.z);

	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);


	/****************************
	/ Add your objects from here
	****************************/
	//add objects

	drawAxes();
	drawGrid();
	drawTorus(torusInnerR, torusOuterR, 24, 30);

	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}


void animate(){
	angle+=0.05;
	//codes for any changes in Models, Camera
	glutPostRedisplay();
}

void init(){
	//codes for initialization
	drawgrid=0;
	drawaxes=1;
	cameraHeight=150.0;
	cameraAngle=1.0;
	angle=0;


	//clear the screen
	glClearColor(0,0,0,0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(80,	1,	1,	1000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
